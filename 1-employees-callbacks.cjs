/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

// const data = {
//     "employees": [
//         {
//             "id": 23,
//             "name": "Daphny",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 73,
//             "name": "Buttercup",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 93,
//             "name": "Blossom",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 13,
//             "name": "Fred",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 89,
//             "name": "Welma",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 92,
//             "name": "Charles Xavier",
//             "company": "X-Men"
//         },
//         {
//             "id": 94,
//             "name": "Bubbles",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 2,
//             "name": "Xyclops",
//             "company": "X-Men"
//         }
//     ]
// }

const fs = require('fs');
const path = require('path');

dataFile = path.resolve('./data.json');
//1. Retrieve data for ids : [2, 13, 23].

fs.readFile(dataFile, 'utf-8', (error, data) => {
    if (error) {
        console.error(error);
    } else {
        const employeesData = JSON.parse(data).employees;
        const reqData = employeesData.filter((employee) => {
            return employee.id === 2 || employee.id === 13 || employee.id === 23;
        });
        //console.log(reqData);
        fs.writeFile('./1Solution.json', JSON.stringify(reqData), (error) => {
            if (error) {
                console.error(error);
            }
        });

        const dataByCompany = employeesData.reduce((accumulator, employee) => {
            let companyName = employee.company;
            if (companyName in accumulator) {
                accumulator[companyName].push(employee);
            }
            else {
                accumulator[companyName] = [employee];
            }
            return accumulator;
        }, {});
        //console.log(dataByCompany);

        fs.writeFile('./2Solution.json', JSON.stringify(dataByCompany), (error) => {
            if (error) {
                console.error(error);
            }
        });

        const companyPowerpuff = dataByCompany['Powerpuff Brigade'];
        //console.log(companyPowerpuff);
        fs.writeFile('./3Solution.json', JSON.stringify(companyPowerpuff), (error) => {
            if (error) {
                console.error(error);
            }
        });
        //4. Remove entry with id 2.
        const dataWithoutid2 = employeesData.filter((employee) => {
            return employee.id !== 2;
        });
        //console.log(dataWithoutid2);
        fs.writeFile('./4Solution.json', JSON.stringify(dataWithoutid2), (error) => {
            if (error) {
                console.error(error);
            }
        });
        //5. Sort data based on company name. If the company name is same, 
        //use id as the secondary sort metric.

        const sortedByCompany = employeesData.sort((employee1, employee2)=>{
            let company1 = employee1.company.toLowerCase();
            let company2 = employee2.company.toLowerCase();
            if(company1 === company2){
                console.log(company1);
                return employee1.id - employee2.id;
            }
            return company2 - company1;
        });
        //console.log(sortedByCompany);
        fs.writeFile('./5Solution.json', JSON.stringify(sortedByCompany), (error) => {
            if (error) {
                console.error(error);
            }
        });

        //6. Swap position of companies with id 93 and id 92.

    }
});